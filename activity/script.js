var staffIds = [100, 101, 102, 103, 104, 105];
// Function 
function checkEmployment(employee) {
    var message;
    staffIds.forEach(function (employeeId) {
        // console.log(employeeId) 
        if (employeeId == employee.id)
            message = "Hi " + employee.name + " enjoy being a " + employee.position + " today!";
    });
    if (typeof message == "undefined")
        message = "They are not an employee";
    return message;
}
var checkingEmployee = checkEmployment({
    id: 101,
    name: "Yunki Min",
    position: "Producer",
    address: "Daegu, South Korea"
});
console.log(checkingEmployee);
var manager = {
    id: 100,
    name: "Bang-PD",
    position: "Manager"
};
console.log(checkEmployment(manager));
