// Type Alias called employeeIds
type employeeIds = Array<number>
let staffIds: employeeIds = [100, 101, 102, 103, 104, 105]

// Employee interface
interface Employee {
    id: number,
    name: string,
    position: string,
    address?: string
}

// Function 
function checkEmployment(employee: Employee): string {
   let message: string;

   staffIds.forEach((employeeId) => {
    // console.log(employeeId) 
    if(employeeId == employee.id) message = "Hi " + employee.name + " enjoy being a " + employee.position + " today!"
    })

   if(typeof message == "undefined") message = "They are not an employee";

    return message;

}

let checkingEmployee = checkEmployment({
    id: 101,
    name: "Yunki Min",
    position: "Producer",
    address: "Daegu, South Korea"
})
console.log(checkingEmployee)

let manager: Employee = {
    id: 100,
    name:"Bang-PD",
    position: "Manager",
}
console.log(checkEmployment(manager))


