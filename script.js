function hello() {
    console.log("Hello, TineScript");
}
hello();
// variables - use type checking and use let keyword, however unlike in Java we don't have to indicate the data types
var myNumber = 1;
// we cannot update a variable to a different type:
// myNumber = "hello";
console.log(myNumber);
// annotations in TS
// annotations in TS, allows us to add and enforce type checking for our variables:
// let varName: annotation;
var myNum;
myNum = 25;
// myNum = true;
var myString;
// myString = {};
var myBool;
// myBool = "true";
// What if we want to create a variable that takes any type?
// let sample = 1;
// sample = "";
var unknownVar;
unknownVar = "gible is my favorite pokemon";
console.log(unknownVar);
unknownVar = 12;
// We could also add annotations to our arrays to limit them to a certain data type:
var myNumArr;
myNumArr = [1, 2, 3];
// myNumArr = [1, "2", 3];
// myNumArr.push("stringmessage")
myNumArr.push(6);
console.log(myNumArr);
// We can still create an array of any type:
var otherArr = ["Yoongi", 30, true];
// We can still use typeof from JS to check the data type of a variable's data:
console.log(typeof unknownVar);
console.log(typeof myNumArr);
// Annotation in Functions
// Annotation can also be used to ensure the data type of the parameter or return value:
function greet(name) {
    return "Hello " + name + " , welcome to the other side!";
}
greet("Jiminie");
function add(num1, num2) {
    return num1 + num2;
}
add(5, 6);
var notification;
var newUser = {
    name: "Prince Ali",
    age: 16
};
var user3 = {
    username: "ksj1992",
    password: "1234",
    age: 15,
    email: "imyksj@mail.com"
};
// type UserAlias = {}
// Interface and Alias in Functions:
function getUser() {
    var user = {
        username: "jjk1997",
        age: 25,
        password: "euphoria",
        email: "mytime@mail.com"
    };
    return user;
}
console.log(getUser());
function getAnimalHabitat(animal) {
    return animal.name + " lives in " + animal.habitat;
}
console.log(getAnimalHabitat({
    name: "Panda",
    habitat: "China"
}));
//array<T> with Type Alias
var zooAnimals = [
    {
        name: "Tiger",
        habitat: "Jungle"
    },
    {
        name: "Orangutan",
        habitat: "Jungle"
    },
    {
        name: "Python",
        habitat: "Jungle"
    }
];
// Array<T> with an Interface
var guildMembers = [
    {
        username: "Jean",
        password: "123456",
        age: 30,
        email: "ksj@mail.com"
    }
];
console.log(guildMembers);
var employeeId1 = "1231211-A";
var employeeId2 = 1231211;
var mylock1 = "locked";
// let myFaveNumber: favoriteNumbers = 44;
var myFaveNumber2 = 13;
var newLock = {
    brand: "Master",
    state: "locked"
};
console.log(newLock);
