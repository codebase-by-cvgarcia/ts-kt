function hello(){
    console.log("Hello, TineScript");
}

hello();

// variables - use type checking and use let keyword, however unlike in Java we don't have to indicate the data types

let myNumber = 1;

// we cannot update a variable to a different type:
// myNumber = "hello";
console.log(myNumber);

// annotations in TS
// annotations in TS, allows us to add and enforce type checking for our variables:
// let varName: annotation;
let myNum: number;
myNum = 25;
// myNum = true;

let myString: string;
// myString = {};

let myBool: boolean;
// myBool = "true";

// What if we want to create a variable that takes any type?
// let sample = 1;
// sample = "";

let unknownVar: any;
unknownVar = "gible is my favorite pokemon";
console.log(unknownVar);
unknownVar = 12;

// We could also add annotations to our arrays to limit them to a certain data type:
let myNumArr: number[];
myNumArr = [1, 2, 3];
// myNumArr = [1, "2", 3];
// myNumArr.push("stringmessage")
myNumArr.push(6)
console.log(myNumArr);

// We can still create an array of any type:
let otherArr: any[] = ["Yoongi", 30, true];

// We can still use typeof from JS to check the data type of a variable's data:
console.log(typeof unknownVar);
console.log(typeof myNumArr)

// Annotation in Functions
// Annotation can also be used to ensure the data type of the parameter or return value:

function greet(name: string){
    return "Hello " + name + " , welcome to the other side!";
}

greet("Jiminie");

function add(num1: number, num2: number){
    return num1 + num2;
}

add(5, 6)

// interface enforce the shape of an object
// ? denotes an optional property in an interface

interface User {
    username: string,
    password: string,
    age: number,
    guildName?: string
}

// let user1: User = {
//     username: "myg1993",
//     password: "1234",
//     age: 15
// }

// let user2: User = {
//     username: "jmp1995",
//     password: "1234",
//     age: 20,
//     guildName: "The Minions"
// }

// Type Alias
// Type alias- allows us to define/create names for different data type:

type Message = string;
let notification: Message;
// notification = true;

// You could also use Type Alias like an interface to enforce conforming an objeect to a certain shape or structure

type UserAlias = {
    name: string;
    age: number;
    address?: string;
    income?: number;
    expenses?: number;
}

let newUser: UserAlias = {
    name: "Prince Ali",
    age: 16
}

interface User {
    email: string;
}

let user3: User = {
    username: "ksj1992",
    password: "1234",
    age: 15,
    email: "imyksj@mail.com"
}

// type UserAlias = {}

// Interface and Alias in Functions:

function getUser(): User {
    let user: User = {
        username: "jjk1997",
        age: 25,
        password: "euphoria",
        email: "mytime@mail.com"
    }

    return user;
}

console.log(getUser())

// We can also use Type Alias to annotate our parameters and enforce the shape of the parameter

type Animal = {
    name: string;
    habitat: string;
}

function getAnimalHabitat(animal: Animal): string {
    return animal.name + " lives in " + animal.habitat;
}

console.log(getAnimalHabitat({
    name: "Panda",
    habitat: "China"
}))

//array<T> with Type Alias

let zooAnimals: Array<Animal> = [
    {
        name: "Tiger",
        habitat: "Jungle"
    }, 
    {
        name: "Orangutan",
        habitat: "Jungle"
    },
    {
        name: "Python",
        habitat: "Jungle"
    }
]

// Array<T> with an Interface

let guildMembers: Array<User> = [
    {
        username: "Jean",
        password: "123456",
        age: 30,
        email: "ksj@mail.com"
    }
]

console.log(guildMembers)

// Union Type:
// Allows us to combine two or more types
// Data types defined in a union type is called union members:

type userId = string | number;

let employeeId1: userId = "1231211-A";
let employeeId2: userId = 1231211;
// let employeeId3: userId = true;

// Explicitly define the expected values as our union type members:

type lockState = "locked" | "unlocked";
let mylock1: lockState = "locked";
// let mylock2: lockState = "broken";
// let mylock3: lockState = true;

type favoriteNumbers = 13 | 23 | 7;
// let myFaveNumber: favoriteNumbers = 44;
let myFaveNumber2: favoriteNumbers = 13;

// union type in an interface

interface LockObject {
    brand: string,
    state: lockState
}

let newLock: LockObject = {
    brand: "Master",
    state: "locked"
}

console.log(newLock)

